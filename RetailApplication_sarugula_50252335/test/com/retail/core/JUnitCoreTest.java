package com.retail.core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.retail.shipping.IShippingCost;
import com.retail.shipping.ShipCost;
import com.retail.shipping.ShippingFactory;

//import com.retail.shipping.ShippingManager;

public class JUnitCoreTest {
	private ItemSupplier itemSupplier;
	private ShipCost sc;
	private ShippingFactory sm;

	@Before
	public void doSetUp() {
		itemSupplier = new ItemSupplier();
		sc = new ShipCost();
		sm = new ShippingFactory();
	}

	@Test
	public void productCountTest() {
		assertEquals(12, itemSupplier.SupplyItems().size(), 0.0001);
	}

	@Test
	public void checkUpcLengthTest() {
		Set<Item> set = itemSupplier.SupplyItems();
		for (Item item : set) {
			// item.UPCLength()
			assertEquals(12, item.UPCLength(), 0.0001);

		}

	}

	@Test
	public void checkUpcLengthNegativeTest() {
		Set<Item> set = itemSupplier.SupplyItems();
		for (Item item : set) {
			// item.UPCLength()
			assertEquals(10, item.UPCLength(), 0.0001);

		}

	}

	@Test

	public void groundShippingCostTest()

	{

		Set<Item> set = sc.calculateShipCost(itemSupplier.SupplyItems());
		List<Object> list = new ArrayList<Object>(set);
		Object[] item = list.toArray();
		Item it = (Item) item[0];
		// Item on test
		// new Item(567321101984L,"CD�Michael Jackson, Thriller",23.88 ,0.50
		// ,"GROUND"));
		// System.out.println(it.getUPC());

		IShippingCost ishippingcost = sm.calculateShipCost(it);

		assertEquals(1.25, ishippingcost.shippingCost(it), 0.001);
	}

	@Test
	public void airShippingCostTest()

	{

		Set<Item> set = sc.calculateShipCost(itemSupplier.SupplyItems());
		List<Object> list = new ArrayList<Object>(set);
		Object[] item = list.toArray();
		Item it = (Item) item[6];
		// Item on test
		//567321101985                    CD �Queen, A Night at the Opera                20.49  0.55        AIR        4.4
		// System.out.println(it.getUPC());

		IShippingCost ishippingcost = sm.calculateShipCost(it);

		assertEquals(4.4, ishippingcost.shippingCost(it), 0.001);
	}

}
