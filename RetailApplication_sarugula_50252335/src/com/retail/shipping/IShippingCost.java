package com.retail.shipping;

import com.retail.core.Item;

public interface IShippingCost {

	

	public double shippingCost(Item item);
}
