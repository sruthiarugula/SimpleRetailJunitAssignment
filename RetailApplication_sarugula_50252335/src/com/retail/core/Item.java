package com.retail.core;


public class Item {

	private Long upc;
	private String description;
	private double price;
	private double weight;
	private String shipMethod;
	private double shippingCost;
	

	public void setUPC(Long uPC) {
		upc = uPC;
	}

	public double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public Item(long uPC, String description, double price, double weight, String shipMethod, double shippingCost) {
		super();

		this.upc = uPC;
		this.setUPC(uPC);
		this.setDescription(description);
		this.setPrice(price);
		this.setWeight(weight);
		this.setShipMethod(shipMethod);
		this.setShippingCost(shippingCost);
	}

	public Item() {

	}

	// Parameterized Constructor
	public Item(long uPC, String description, double price, double weight, String shipMethod) {
		super();
		
		this.setUPC(uPC);
		this.setDescription(description);
		this.setPrice(price);
		this.setWeight(weight);
		this.setShipMethod(shipMethod);
	}

	public long getUPC() {
		return upc;
	}

	public void setUPC(long uPC) {
		this.upc = uPC;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getShipMethod() {
		return shipMethod;
	}

	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}

	public int UPCLength() {

		long UPCLength = this.getUPC();
		String s = String.valueOf(UPCLength);

		return s.length();

	}

}
